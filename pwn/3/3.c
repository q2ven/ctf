#include <stdio.h>


void flag(void) {
  printf("FLAG\n");
}

void overflow(void) {
  char buf[16];

  printf("Input: ");
  scanf("%s", buf);
  printf("%s\n", buf);
}

int main(int argc, char *argv[]) {
  overflow();

  return 0;
}
