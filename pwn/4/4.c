#include <stdio.h>


void flag(int overflown) {
  if (overflown == 0x11223344) {
    printf("FLAG\n");
  }
}

void overflow(void) {
  char buf[16];

  printf("Input: ");
  scanf("%s", buf);
  printf("%s\n", buf);
}

int main(int argc, char *argv[]) {
  overflow();

  return 0;
}
