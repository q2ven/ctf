# kindvm

最近CTFの難易度が上がったと言われるが、同時に親切なWrite Upも減った気がするので、できるだけ丁寧に書く。考えたことも多めに。高校生の自分が分かるくらいに丁寧に。あくまで目標。

# 問題

kindvmの実行ファイルがダウンロードでき、問題分として以下の文章が与えられていた。

> Get hints, and pwn it! kindvm.pwn.seccon.jp 12345

とりあえずヒント集める必要がありそう（あほ）。パスワードも無いし`ssh`より`nc`で接続すればいいのかなと思った。

# 前準備

## 調査

まずは`file`コマンドでファイルの種別を調べる。

```sh
$ file kindvm_79726158fec11eb1e5a89351db017e13506d3a4a
kindvm_79726158fec11eb1e5a89351db017e13506d3a4a: ELF 32-bit LSB executable, Intel 80386, version 1 (SYSV), dynamically linked, interpreter /lib/ld-linux.so.2, for GNU/Linux 2.6.32, BuildID[sha1]=8f9180cac21d47474ed903aea39dd56c7dc9f495, not stripped
```

どうやらリトルエンディアンの32ビットマシンの実行ファイルのようだ。32ビットなあたりkindさを感じる。

実行ファイルなので実行権限を与えて実行する。

```sh
$ chmod +x kindvm_79726158fec11eb1e5a89351db017e13506d3a4a
$ ./kindvm_79726158fec11eb1e5a89351db017e13506d3a4a
Input your name : test
Input instruction :
Oops! Sorry, it's TIME OUT !
```

名前を入力した後に、命令を入力しろと言われて悩んでいたらタイムアウトになった。入力とタイマーが非同期に動作しているので、おそらくシグナルハンドラかなと思った。(マルチプロセス、マルチスレッドの可能性も捨てきれないが、わざわざ書かないだろうと思った。そう思うくらいシグナルを使えば簡単に書ける。)

何回か実行したところ、だいたい実行から5秒でタイムアウトになる。`alarm`関数じゃね。。。？

*****
*****

余談。


OSはそもそも並列処理を擬似的に実装している。ものすごい短い間だけプログラムを実行して、別のプログラムに切り替える。OSはすべてのプログラムをちょっとずつ進めるが、切り替える時間が短いために、人間が並列に実装できていると体感する。

要はOSがユーザーのプログラムに割り込む余地が存在する。すぐにプロセスを切り替えるので、キーボードの入力を監視してCtrl+Cでプログラムを止めたりもできる。

Ctrl+Cが押されると`SIGINT`というシグナルが予め設定されたシグナルハンドラに渡される。(デフォルトでは)このシグナルハンドラは`SIGINT`シグナルを受け取った時に、プログラムを終了するという関数である。従ってCtrl+Cでプログラムを強制終了できる。

他にも`SIGALRM`というシグナルが存在する。`alarm`関数に秒数を渡して実行すると、OSがその秒数後に`SIGALRM`を送出してくれる。今回はそのシグナルハンドラを弄ったのだと思った。

このシグナルハンドラはユーザーが好きに設定でき、ユーザーが自由に使えるシグナルも2つある。

シグナルについて、詳しくは[ここらへん](https://harasou.jp/2017/01/23/linux-signal/)読むといいかも。できたら[man signal](https://linuxjm.osdn.jp/html/LDP_man-pages/man7/signal.7.html)とかも読んでほしい。


*****
*****

## オーバーフロー

とりあえず入力があったら大量の文字を入力してみる。ガンガン入力する。ただし5秒の制約があることに注意する。長押しすればいいだけ。

```sh
$ ./kindvm_79726158fec11eb1e5a89351db017e13506d3a4a
Input your name : ああああああああああああああああああああああああああああああああああ
$
```

あっさり死んだ。この後しばらくしてから気づいたが、謎のファイルが生成されていた。ヒント来た〜と思ったけど空ファイルだった。

```sh
$ ls
hint1.txt  kindvm_79726158fec11eb1e5a89351db017e13506d3a4a
$ cat hint1.txt
$
```

手元で実行するのがよくないのかと思い、`nc`コマンドでサーバーに接続してみた。すると接続した瞬間に同じプログラムが実行され、オーバーフローさせるとヒントが表示された。

```
$ nc kindvm.pwn.seccon.jp 12345
Input your name : aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa
 _   _ _       _   _    ____ _____ _____   _ 
| | | (_)_ __ | |_/ |  / ___| ____|_   _| | |
| |_| | | '_ \| __| | | |  _|  _|   | |   | |
|  _  | | | | | |_| | | |_| | |___  | |   |_|
|_| |_|_|_| |_|\__|_|  \____|_____| |_|   (_)
                                             

Nice try! The theme of this binary is not Stack-Based BOF!
However, your name is not meaningless...
```

どうもスタックオーバーフローの問題では無いらしい。ただし`名前の入力には意味がある`そう。一応念頭に置いておく。

今回学んだのは、***実行環境が用意されている場合はその環境で実行するべき***、だということ。実行ファイルはあくまで解析用、実行は用意された環境ですべきなのだろう。


# 静的解析ときどき動的

ここまでで完全に手詰まりなので、静的解析をする。`objdump`コマンドでアセンブリを読むことになる。まぁでもソースコード？やから？全部読めればただのプログラムと一緒やん？って感じのモチベーションで読んだ（ハイパーあほ）。

```sh
$ objdump -d kindvm_79726158fec11eb1e5a89351db017e13506d3a4a | more

kindvm_79726158fec11eb1e5a89351db017e13506d3a4a:     ファイル形式 elf32-i386


セクション .init の逆アセンブル:

08048528 <_init>:
 8048528:	53                   	push   %ebx
 8048529:	83 ec 08             	sub    $0x8,%esp
...
```

こんな感じで出力されるアセンブリを読んでいく。pdfに出力して印刷したら26ページだった。

まずは最初の方から読んでいく。

## .plt

`plt`のセクションには共有ライブラリの関数(`stdio.h`とかで定義される関数とか)が、実際どこのアドレスに格納されているかが保持される(と思っている)。例えば`*0x0804b00c`に`read`関数の開始地点のアドレスが入っている。(間違ってたらごめんなさい。)

```
セクション .plt の逆アセンブル:

08048550 <.plt>:
 8048550:	ff 35 04 b0 04 08    	pushl  0x804b004
 8048556:	ff 25 08 b0 04 08    	jmp    *0x804b008
 804855c:	00 00                	add    %al,(%eax)
	...

08048560 <read@plt>:
 8048560:	ff 25 0c b0 04 08    	jmp    *0x804b00c
 8048566:	68 00 00 00 00       	push   $0x0
 804856b:	e9 e0 ff ff ff       	jmp    8048550 <.plt>
...
08048590 <signal@plt>:
 8048590:	ff 25 18 b0 04 08    	jmp    *0x804b018
 8048596:	68 18 00 00 00       	push   $0x18
 804859b:	e9 b0 ff ff ff       	jmp    8048550 <.plt>

080485a0 <lseek@plt>:
 80485a0:	ff 25 1c b0 04 08    	jmp    *0x804b01c
 80485a6:	68 20 00 00 00       	push   $0x20
 80485ab:	e9 a0 ff ff ff       	jmp    8048550 <.plt>

080485b0 <alarm@plt>:
 80485b0:	ff 25 20 b0 04 08    	jmp    *0x804b020
 80485b6:	68 28 00 00 00       	push   $0x28
 80485bb:	e9 90 ff ff ff       	jmp    8048550 <.plt>
...
```

注目すべきは`signal`と`alarm`が入っていること。これらが使われるのだから間違いなく最初の予想が的中したと確信した。天才かなと思った。

これ以外の部分に関しては特に何もないので読み流す。

## 読み飛ばす部分

以下の関数はだいたいのプログラムに存在し、プログラムをセットアップする処理だと思っている。`main`関数を呼び出したり、その準備をしてそうな気がする。

```
08048680 <_start>:
080486b0 <__x86.get_pc_thunk.bx>:
080486c0 <deregister_tm_clones>:
080486f0 <register_tm_clones>:
08048730 <__do_global_dtors_aux>:
08048750 <frame_dummy>:
...
08049120 <__libc_csu_init>:
08049180 <__libc_csu_fini>:
08049184 <_fini>:
```

## main関数

ここから実際の処理が始まる。

```
0804877b <main>:
 804877b:	8d 4c 24 04          	lea    0x4(%esp),%ecx
 804877f:	83 e4 f0             	and    $0xfffffff0,%esp
 8048782:	ff 71 fc             	pushl  -0x4(%ecx)
 8048785:	55                   	push   %ebp
 8048786:	89 e5                	mov    %esp,%ebp
 8048788:	51                   	push   %ecx
 8048789:	83 ec 04             	sub    $0x4,%esp
 804878c:	e8 9a 08 00 00       	call   804902b <ctf_setup>
 ...
```

*****
*****

余談。

### espの退避

現在のスタックは以下のようになっているはずで、`esp + 4`は第一引数(一般に`argc`)を指している。

|stack|...|
|:--:|:--:|
|mainの戻りアドレス|espがここを指している|
|argc||
|argv||
|argv[1]||
|...|

これから4を引いた値、つまり`esp`と、そのままの`argc`のアドレスをプッシュしている。

```
 804877b:	8d 4c 24 04          	lea    0x4(%esp),%ecx  第一引数のアドレス
...
 8048782:	ff 71 fc             	pushl  -0x4(%ecx)  なぜかespを退避している
...
 8048788:	51                   	push   %ecx  第一引数のアドレスをプッシュ
```

意味が分からない。

(あと問題とは関係ない。)

### スタックのアラインメント

32bitアーキテクチャなら4バイトずつ読み出せるため、メモリに変数などを4バイトずつ配置すると無駄なく高速に読み出せる。したがって`and`演算をして`esp`が一番近い4バイト境界を指すようにする。下位8ビットが0になり、スタックが低位に伸びる、したがって領域が確保される。(もちろん元々0の場合は確保されない。)

```
 804877f:	83 e4 f0             	and    $0xfffffff0,%esp
```

### EBPの退避と設定

関数を呼び出す際、まず最初に引数をスタックにプッシュする。そして関数の次に実行される命令のアドレス、つまり戻りアドレスをプッシュする。そして呼び出された関数の中で、前の関数で使用していた値をスタックに退避する。

ほとんどの場合、まず最初に`ebp`の値を退避する。そして現在の`esp`を`ebp`に代入する。
```
 8048785:	55                   	push   %ebp
 8048786:	89 e5                	mov    %esp,%ebp
```

この時のスタックの内容は以下のようになっている。

|stack|...|
|:--:|:--:|
|プッシュされたebp|espとebpがここを指している|
|プッシュされたesp||
|...|and命令で確保される領域|
|mainの戻りアドレス||
|argc||
|argv||
|argv[1]||
|...|

`ebp`はその関数内で使用するスタックの底のアドレスを指しており、`ebp + ?`で引数にアクセスできるようになっている。この`ebp`は関数毎に異なるので、退避させる必要がある。

この話は[ここ](https://note.mu/nekotricolor/n/n2a247c808275)とかを読むといいかも。

*****
*****

少し読み飛ばすと、いくつか関数が呼ばれているので、これを見ていく。

```
 804878c:	e8 9a 08 00 00       	call   804902b <ctf_setup>
 8048791:	e8 5a 00 00 00       	call   80487f0 <kindvm_setup>
 8048796:	e8 28 02 00 00       	call   80489c3 <input_insn>
```

## ctf_setup関数

結構後ろの方にある。

まず気になったのは`setvbuf`という関数が呼ばれていたこと。そしてその第一引数(関数の直前でプッシュされた値)は、`0x0804b080`という値。直接指定されているのでグローバル変数かなにか？とか考えたがよく分からない。

```
0804902b <ctf_setup>:
...
 8049031:	a1 80 b0 04 08       	mov    0x804b080,%eax
 8049036:	6a 00                	push   $0x0
 8049038:	6a 02                	push   $0x2
 804903a:	6a 00                	push   $0x0
 804903c:	50                   	push   %eax
 804903d:	e8 ee f5 ff ff       	call   8048630 <setvbuf@plt>
...
```

一応関数について調べると、ストリームに対してバッファ領域の設定をする関数のよう。いまいち理解できずに読み飛ばした。(この後同様に2回呼び出される。)

`ctf_setup`関数の最後の方を見ると以下のような部分がある。

```
 8049070:	68 0b 90 04 08       	push   $0x804900b
 8049075:	6a 0e                	push   $0xe
 8049077:	e8 14 f5 ff ff       	call   8048590 <signal@plt>
 804907c:	83 c4 10             	add    $0x10,%esp
 804907f:	83 ec 0c             	sub    $0xc,%esp
 8049082:	6a 05                	push   $0x5
 8049084:	e8 27 f5 ff ff       	call   80485b0 <alarm@plt>
```

`0x0804900b`は`signal_handler_timeout`という関数を指しており、この部分は次のように見えた。

```c
    signal(SIGALRM, signal_hander_timeout);
    alarm(5);
```

実際、`SIGALRM`は`signal.h`で以下のように定義され、`0xe`だと分かる。

```c
#define SIGALRM         14
```

ちなみに`signal_handler_timeout`関数は以下のように定義されるが、重要なのは`exit`関数が呼び出されているということだけで、他は読んでいない。

```
0804900b <signal_handler_timeout>:
 804900b:	55                   	push   %ebp
 804900c:	89 e5                	mov    %esp,%ebp
 804900e:	83 ec 08             	sub    $0x8,%esp
 8049011:	83 ec 0c             	sub    $0xc,%esp
 8049014:	68 48 92 04 08       	push   $0x8049248
 8049019:	e8 b2 f5 ff ff       	call   80485d0 <puts@plt>
 804901e:	83 c4 10             	add    $0x10,%esp
 8049021:	83 ec 0c             	sub    $0xc,%esp
 8049024:	6a ff                	push   $0xffffffff
 8049026:	e8 b5 f5 ff ff       	call   80485e0 <exit@plt>
```

ちなみに`exit`の部分は、

```c
    exit(-1);
```

こう見える。

`ctf_setup`で大事なのはタイムアウトの設定がされていることくらいだと思う。

ちなみに`alarm`関数の使い方なんかは、[ここ](http://blog.majide.com/2009/03/c-alarm-sample-code/)を参考にするとよい。


## kindvm_setup関数

名前的に大事そう。しっかり読もう。

```
 80487fa:	6a 18                	push   $0x18
 80487fc:	e8 bf fd ff ff       	call   80485c0 <malloc@plt>
 8048801:	83 c4 10             	add    $0x10,%esp
 8048804:	a3 e8 b0 04 08       	mov    %eax,0x804b0e8
```

まず24バイトの領域を確保している。返り値は`eax`に入っている(gccはそうしている)。そして返り値、つまりヒープ領域に確保された24バイトの先頭アドレスを`0x0804b0e8`に代入している。`int`型の領域か何かかな？と思った。24文字の文字列の領域をわざわざ`malloc`関数で確保しないと思うので、多分`char`型ではないと思う。

```c
24 bytes = 4 bytes * 6 = sizeof(int) * 6
24 bytes = 1 bytes * 24 = sizeof(char) * 24
```

こんなイメージ。

```c
    int *tmp;
    tmp = malloc(sizeof(int) * 6);
    printf("%a\n", &a);  /* このtmpのアドレスが0x0804b0e8 */
```

次にこの変数のアドレスを`eax`に入れ、そこに0を代入している。次に同様に0を`eax + 4`の位置に代入している。

```
 8048809:	a1 e8 b0 04 08       	mov    0x804b0e8,%eax
 804880e:	c7 00 00 00 00 00    	movl   $0x0,(%eax)
 8048814:	a1 e8 b0 04 08       	mov    0x804b0e8,%eax
 8048819:	c7 40 04 00 00 00 00 	movl   $0x0,0x4(%eax)
```

多分こんな感じ？

```c
    *tmp = 0;
    *(tmp + 4) = 0;
```

ここまでをヒープでみるとこんな感じのはず。

|heap|address||
|:--:|:--:|:--|
|...|...|ヒープは高位に伸びる|
|0|*0x0804b0e8|tmp|
|0|*0x0804b0e8 + 4|tmp + 4|
|?|*0x0804b0e8 + 8|tmp + 8|
|?|*0x0804b0e8 + 12|tmp + 12|
|?|*0x0804b0e8 + 16|tmp + 16|
|?|*0x0804b0e8 + 20|tmp + 20|

次にさっきと同じ値を`ebx`に入れ、`input_username`関数の返り値を`ebx + 8`の場所に代入している。

```
 8048820:	8b 1d e8 b0 04 08    	mov    0x804b0e8,%ebx
 8048826:	e8 12 01 00 00       	call   804893d <input_username>
 804882b:	89 43 08             	mov    %eax,0x8(%ebx)
```

入力した結果を`malloc`関数で確保した領域に入れてそのアドレスを代入していると思った。

```c
    *(tmp + 8) = input_username();
```

気になったので少しだけ眺める。

### input_username関数

最初のほうを見ると`printf`などの処理があるが、ここらへんは正直どうでもいい。

```
0804893d <input_username>:
 804893d:	55                   	push   %ebp
 804893e:	89 e5                	mov    %esp,%ebp
 8048940:	83 ec 28             	sub    $0x28,%esp
 8048943:	65 a1 14 00 00 00    	mov    %gs:0x14,%eax
 8048949:	89 45 f4             	mov    %eax,-0xc(%ebp)
 804894c:	31 c0                	xor    %eax,%eax
 804894e:	83 ec 0c             	sub    $0xc,%esp
 8048951:	68 bd 91 04 08       	push   $0x80491bd
 8048956:	e8 15 fc ff ff       	call   8048570 <printf@plt>
 804895b:	83 c4 10             	add    $0x10,%esp
 804895e:	83 ec 0c             	sub    $0xc,%esp
 8048961:	8d 45 ea             	lea    -0x16(%ebp),%eax
 8048964:	50                   	push   %eax
 8048965:	e8 16 fc ff ff       	call   8048580 <gets@plt>
```

気になるのはここから。`gets`関数で標準入力の内容を`-0x16(%ebp)`に読み出している。

```
 8048961:	8d 45 ea             	lea    -0x16(%ebp),%eax
 8048964:	50                   	push   %eax
 8048965:	e8 16 fc ff ff       	call   8048580 <gets@plt>
```

次に`malloc`関数で`0xa`、10文字分の領域を確保し、そのアドレスを`-0x20(%ebp)`に保存している。

```
 804896a:	83 c4 10             	add    $0x10,%esp
 804896d:	83 ec 0c             	sub    $0xc,%esp
 8048970:	6a 0a                	push   $0xa
 8048972:	e8 49 fc ff ff       	call   80485c0 <malloc@plt>
 8048977:	83 c4 10             	add    $0x10,%esp
 804897a:	89 45 e0             	mov    %eax,-0x20(%ebp)
```

そして標準入力から読み取った文字列の長さを`strlen`関数で取得し、それを`-0x1c(%ebp)`に保存している。

```
 8048980:	8d 45 ea             	lea    -0x16(%ebp),%eax
 8048983:	50                   	push   %eax
 8048984:	e8 77 fc ff ff       	call   8048600 <strlen@plt>
  8048989:	83 c4 10             	add    $0x10,%esp
 804898c:	89 45 e4             	mov    %eax,-0x1c(%ebp)
```

その後`strncpy`関数の第一引数に`malloc`関数で確保した領域、第二引数に標準入力の内容を、第三引数に`strlen`関数で得た文字列の長さを渡している。

```
 804899b:	ff 75 e4             	pushl  -0x1c(%ebp)
 804899e:	8d 45 ea             	lea    -0x16(%ebp),%eax
 80489a1:	50                   	push   %eax
 80489a2:	ff 75 e0             	pushl  -0x20(%ebp)
 80489a5:	e8 a6 fc ff ff       	call   8048650 <strncpy@plt>
```

`strncpy`関数では第一引数に第二引数の文字列をコピーする関数だから、標準入力の内容を`malloc`関数で確保した領域にコピーしていることになる。

関数の終盤では`malloc`関数で確保した領域を`eax`に入れている。つまりこれが返り値となる。

```
 80489ad:	8b 45 e0             	mov    -0x20(%ebp),%eax
```

ここまでをcで書くとおおよそ以下のようになると思う。

```c
char * input_username(void) {
    char input[LENGTH_I_DONT_KNOW];
    char *username;

    gets(input);

    username = (char *)malloc(sizeof(char) * 10);

    strncpy(username, input, strlen(input));

    return username;
}
```

`input_username`関数が終わる直前のこの部分を見ると、[スタックカナリア](https://www.ipa.go.jp/security/awareness/vendor/programmingv2/contents/c905.html)を思い出した。実際、カナリアを設定した場合はこの`__stack_chk_fail`関数がアセンブリに出現する(気がする)。

```
80489bc:	e8 2a 06 00 00       	call   8048feb <__stack_chk_fail>
```

つまりオーバーフローが起きた時の挙動はここで定義されている気がした。確かに`exit`関数が呼ばれていた。

```
08048feb <__stack_chk_fail>:
 8048feb:	55                   	push   %ebp
 8048fec:	89 e5                	mov    %esp,%ebp
 8048fee:	83 ec 08             	sub    $0x8,%esp
 8048ff1:	83 ec 0c             	sub    $0xc,%esp
 8048ff4:	68 3e 92 04 08       	push   $0x804923e
 8048ff9:	e8 91 00 00 00       	call   804908f <open_read_write>
 8048ffe:	83 c4 10             	add    $0x10,%esp
 8049001:	83 ec 0c             	sub    $0xc,%esp
 8049004:	6a ff                	push   $0xffffffff
 8049006:	e8 d5 f5 ff ff       	call   80485e0 <exit@plt>
```

少し気になるのはオーバーフローした際に生成された`hint1.txt`というファイルと`open_read_write`という関数名。なにか関係がありそう。おまけに引数を取っているから、渡したファイル名を基にその内容を読み出してどこかに書き込んでいるのかなと思った。

```
$ gdb kindvm_79726158fec11eb1e5a89351db017e13506d3a4a
...
(gdb) handle SIGALRM ignore
Signal        Stop	Print	Pass to program	Description
SIGALRM       No	No	No		Alarm clock
(gdb) break *0x08048ff4
Breakpoint 1 at 0x8048ff4
(gdb) run
Starting program: /ctf/seccon/kindvm/kindvm_79726158fec11eb1e5a89351db017e13506d3a4a
Input your name : aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa

Breakpoint 1, 0x08048ff4 in __stack_chk_fail ()
(gdb) x/1s 0x0804923e
0x804923e:	"hint1.txt"
```

`gdb`でタイムアウト対策でシグナルを無効にし、`open_read_write`の直前にブレイクポイントを張った。そしてオーバーフローさせたタイミングで`0x0804923e`の中身を確認すると、`hint1.txt`だった。

### open_read_write関数

ここも気になったので読んでみる。やはり`ebp + 8`のこの関数の第一引数、ファイル名を`open`関数に引数として渡している。

```
0804908f <open_read_write>:
 804908f:	55                   	push   %ebp
 8049090:	89 e5                	mov    %esp,%ebp
 8049092:	83 ec 18             	sub    $0x18,%esp
 8049095:	83 ec 08             	sub    $0x8,%esp
 8049098:	6a 72                	push   $0x72
 804909a:	ff 75 08             	pushl  0x8(%ebp)
 804909d:	e8 4e f5 ff ff       	call   80485f0 <open@plt>
```

その後に`read`関数が呼ばれ、`write`関数で`0x1`のファイルディスクリプタ、標準出力に書き込まれている。

```
 80490ec:	e8 6f f4 ff ff       	call   8048560 <read@plt>
...
 80490fd:	6a 01                	push   $0x1
 80490ff:	e8 1c f5 ff ff       	call   8048620 <write@plt>
```

つまりこの関数はファイル名を引数にとり、その内容を表示するという関数だった。

## kindvm_setup その2

`kindvm_setup`では24バイトの領域を確保して、そこに値を設定してる最中だった。`input_username`関数を読んで、その24バイトの領域が以下のようになることが分かった。

|heap|address||
|:--:|:--:|:--|
|...|...|ヒープは高位に伸びる|
|0|*0x0804b0e8|tmp|
|0|*0x0804b0e8 + 4|tmp + 4|
|ヒープに確保された領域のアドレス|*0x0804b0e8 + 8|tmp + 8, ユーザーネームの先頭アドレス|
|?|*0x0804b0e8 + 12|tmp + 12|
|?|*0x0804b0e8 + 16|tmp + 16|
|?|*0x0804b0e8 + 20|tmp + 20|
|...||mallocでここらにユーザーネームの領域が確保されているはず|

この次に`0x080491b2`の値を続いて代入している。これを同様に`gdb`コマンドで調べたところ`banner.txt`だった。

```
 804882e:	a1 e8 b0 04 08       	mov    0x804b0e8,%eax
 8048833:	c7 40 0c b2 91 04 08 	movl   $0x80491b2,0xc(%eax)
```

どうせこれも出力されると思ったので、名前的に最初に表示されるこれかと思った。

```
 _    _           _                 
| | _(_)_ __   __| |_   ___ __ ___  
| |/ / | '_ \ / _` \ \ / / '_ ` _ \ 
|   <| | | | | (_| |\ V /| | | | | |
|_|\_\_|_| |_|\__,_| \_/ |_| |_| |_|

```

残りの8バイトには2つのアドレス`0x08048f89`、`0x08048fba`が代入されている。

```
 804883a:	a1 e8 b0 04 08       	mov    0x804b0e8,%eax
 804883f:	c7 40 10 89 8f 04 08 	movl   $0x8048f89,0x10(%eax)
 8048846:	a1 e8 b0 04 08       	mov    0x804b0e8,%eax
 804884b:	c7 40 14 ba 8f 04 08 	movl   $0x8048fba,0x14(%eax)
```

このアドレスはそれぞれ`func_greeting`、`func_farewell`関数の開始アドレスだった。この関数のどちらも`__stack_chk_fail`関数と同様に第一引数のファイルに対して`open_read_write`関数を呼び出していた。

```
08048f89 <func_greeting>:
08048fba <func_farewell>:
```

ここまでをc言語で書くとこんな感じ。う〜〜んカオス！
読み方のポイントとしては、最初が返り値、(*)が関数ポインタを意味して、次のカッコが引数。

```c
    char banner[] = "banner.txt";
    *(tmp + 12) = (int *)banner;
    *(tmp + 16) = (char * (*)(char *))func_greeting;  /* charのポインタを受け取って */
    *(tmp + 20) = (char * (*)(char *))func_greeting;  /* charのポインタを返す関数ポインタへのキャスト */
```

関数のアドレスがぴったり収まるから4バイトの変数なのかと少し納得した。

それでヒープ領域はこんな感じのはず。

|heap|address||
|:--:|:--:|:--|
|...|...|ヒープは高位に伸びる|
|0|*0x0804b0e8|tmp|
|0|*0x0804b0e8 + 4|tmp + 4|
|ヒープに確保された領域のアドレス|*0x0804b0e8 + 8|tmp + 8, ユーザーネームの先頭アドレス|
|banner.txtの先頭アドレス|*0x0804b0e8 + 12|tmp + 12|
|func_greeting関数|*0x0804b0e8 + 16|tmp + 16|
|func_farewell関数|*0x0804b0e8 + 20|tmp + 20|
|...||mallocでここらにユーザーネームの領域が確保されているはず|

一旦ここまでを`gdb`で解析して答え合わせする。とりあえず`func_farewell`関数が代入されるところでブレイクポイントを張って実行。

```
(gdb) break *0x0804884b
Breakpoint 2 at 0x804884b
(gdb) run
Starting program: /ctf/seccon/kindvm/kindvm_79726158fec11eb1e5a89351db017e13506d3a4a
Input your name : aaaaa

Breakpoint 2, 0x0804884b in kindvm_setup ()
(gdb) x/24x *0x0804b0e8
0x804c160:	0x00	0x00	0x00	0x00	0x00	0x00	0x00	0x00
0x804c168:	0x80	0xc1	0x04	0x08	0xb2	0x91	0x04	0x08
0x804c170:	0x89	0x8f	0x04	0x08	0x00	0x00	0x00	0x00
```

最初の4バイトは0、次も0でここまではOK。次の4バイトはユーザーネームのはず。リトルエンディアンなので最下位バイトから保存されていることに注意して、

```
(gdb) x/1s 0x0804c180
0x804c180:	"aaaaa"
(gdb) x/1s 0x080491b2
0x80491b2:	"banner.txt"
```

OK。その次はbanner.txt。その後のアドレスは`0x08048f89`なので`func_greeting`関数であっている。

これでこの24バイトの領域は把握できた。

ちなみにこの24バイトのアドレスは`0x0804c160`から始まり`0x0804ca77`に終わる。一方で`0x0804c180`からユーザーネームが格納されているので、やはり24バイトの領域よりも高位にユーザーネームの領域が確保されていることが分かる。


*****
*****

余談。

関数ポインタの何が嬉しいのか？pythonだと意識せず使う人もいるかもしれない。

```python
# すべての値を2倍する
# map関数にlambdaの無名関数を渡している
a = [1, 2, 3]
b = map(lambda x: x * 4, a)
print(list(b))  # [2, 4, 6]

# 降順にソートする
a.sort(key=lambda x: -x)
print(a)  # [3, 2, 1]
```

他にはGUIのコールバック関数(例えば右クリックした時にメニューを描画する関数)とか。

意外とよく使われている。

*****
*****

読み進めると、以下の部分で1024バイトの領域を確保し、それを`0x0804b0a0`というアドレスに入れている。1024バイトだと長さ256の`int`型配列か1024の`char`型配列が考えられる。でもそんな大きい`int`型配列使う？使わんよな、と思うので、多分`char`型？

```
 8048855:	68 00 04 00 00       	push   $0x400
 804885a:	e8 61 fd ff ff       	call   80485c0 <malloc@plt>
 804885f:	83 c4 10             	add    $0x10,%esp
 8048862:	a3 a0 b0 04 08       	mov    %eax,0x804b0a0
```

次に`memset`関数で確保した領域を`NULL`(0)で埋めている。

```
 8048867:	a1 a0 b0 04 08       	mov    0x804b0a0,%eax
 804886c:	83 ec 04             	sub    $0x4,%esp
 804886f:	68 00 04 00 00       	push   $0x400
 8048874:	6a 00                	push   $0x0
 8048876:	50                   	push   %eax
 8048877:	e8 c4 fd ff ff       	call   8048640 <memset@plt>
```

同様に32バイトの領域を確保して`0x0804b0ec`に代入し、`NULL`で初期化している。

```
 8048882:	6a 20                	push   $0x20
 8048884:	e8 37 fd ff ff       	call   80485c0 <malloc@plt>
 8048889:	83 c4 10             	add    $0x10,%esp
 804888c:	a3 ec b0 04 08       	mov    %eax,0x804b0ec
 8048882:	6a 20                	push   $0x20
 8048884:	e8 37 fd ff ff       	call   80485c0 <malloc@plt>
 8048889:	83 c4 10             	add    $0x10,%esp
 804888c:	a3 ec b0 04 08       	mov    %eax,0x804b0ec
```

そこからまた1024バイトの領域を確保して、`0x0804b0f0`に保存している。

```
 80488a9:	68 00 04 00 00       	push   $0x400
 80488ae:	e8 0d fd ff ff       	call   80485c0 <malloc@plt>
 80488b3:	83 c4 10             	add    $0x10,%esp
 80488b6:	a3 f0 b0 04 08       	mov    %eax,0x804b0f0
```

その後、最初に確保した1024バイトの領域を`0x41`で埋めなおしている。`0x41`は[asciiコード](https://www.k-cube.co.jp/wakaba/server/ascii_code.html)で大文字のAを表す。

```
 80488c3:	68 00 04 00 00       	push   $0x400
 80488c8:	6a 41                	push   $0x41
 80488ca:	50                   	push   %eax
 80488cb:	e8 70 fd ff ff       	call   8048640 <memset@plt>
```

どういう意味があるのかわからないが、gdbでヒープを覗いた時に分かりやすくしているのかなと思った。

ここまでをcで書くとこうなる。

```c
    char *tmp2;  /* 0x0804b0a0*/

    tmp2 = (char *)malloc(sizeof(char) * 1024);
    memset(tmp2, 0, sizeof(tmp2));

    char *tmp3; /* 0x0804b0ec */
    char *tmp4; /* 0x0804b0f0 */

    tmp3 = (char *)malloc(sizeof(char) * 32);
    tmp4 = (char *)malloc(sizeof(char) * 1024);

    memset(tmp2, 'A', sizeof(tmp2));
```

kindvm_setupって名前からvm用のメモリとか？わからん。

その次の部分では`0x0804b0c0`というアドレスから4バイト置きにアドレスを代入している。

```
 80488d3:	c7 05 c0 b0 04 08 84 	movl   $0x8048c84,0x804b0c0
 80488da:	8c 04 08 
 80488dd:	c7 05 c4 b0 04 08 9d 	movl   $0x8048c9d,0x804b0c4
 80488e4:	8c 04 08 
 80488e7:	c7 05 c8 b0 04 08 f4 	movl   $0x8048cf4,0x804b0c8
 80488ee:	8c 04 08 
 80488f1:	c7 05 cc b0 04 08 49 	movl   $0x8048d49,0x804b0cc
 80488f8:	8d 04 08 
 80488fb:	c7 05 d0 b0 04 08 98 	movl   $0x8048d98,0x804b0d0
 8048902:	8d 04 08 
 8048905:	c7 05 d4 b0 04 08 3a 	movl   $0x8048e3a,0x804b0d4
 804890c:	8e 04 08 
 804890f:	c7 05 d8 b0 04 08 a4 	movl   $0x8048ea4,0x804b0d8
 8048916:	8e 04 08 
 8048919:	c7 05 dc b0 04 08 b6 	movl   $0x8048eb6,0x804b0dc
 8048920:	8e 04 08 
 8048923:	c7 05 e0 b0 04 08 ed 	movl   $0x8048eed,0x804b0e0
 804892a:	8e 04 08 
 804892d:	c7 05 e4 b0 04 08 3b 	movl   $0x8048f3b,0x804b0e4
```

最初の`0x08048c84`を見てみると`insn_nop`という関数だった。

```
08048c84 <insn_nop>:
```

インストラクション、NOP、多分kindvmのNOP命令?

(NOP命令は何もしない命令、基本的には命令の実行サイクルを調整してハザードを起こさないように使用する。[ここ](http://www.arch.cs.titech.ac.jp/lecture/2009-archII/NxLec-2009-11-09.pdf)の資料が良さげ。分からない人は計算機アーキテクチャの勉強をしよう。そこらへんちゃんと理解しないと静的解析は難しいかも？)

ほかのアドレスも調べるとすべて関数のアドレスだった。

```
08048c84 <insn_nop>:
08048c9d <insn_load>:
08048cf4 <insn_store>:
08048d49 <insn_mov>:
08048d98 <insn_add>:
08048e3a <insn_sub>:
08048ea4 <insn_halt>:
08048eb6 <insn_in>:
08048eed <insn_out>:
08048f3b <insn_hint>:
```

やっぱりvmの命令っぽい、ひとつを除いて。`insn_hint`だけは明らかに違う関数で、これを呼び出せばまたヒントが得られるのかなと思うけど、呼び出し方がまだ分からないので一旦保留。

4バイト毎に入れてるのはこんなイメージ？


```c
    (void) (*func_table[10])(void) = {
        insn_nop,
	insn_load,
	insn_store,
	...
	insn_hint,
    };  /* vtableの最初の先頭アドレスが0x0804b0c0 */
```

なんとなく引数と返り値なしにしてるけど、レジスタとかはこの関数郡から読み込めるスコープで定義されてると思ったから。確証はないけど今回はイメージできればそれで十分。

これで`kindvm_setup`関数は終わり。

まとめると

* 色々な値を含んだ4バイト * 6の領域の確保
  * それとユーザーネームの領域の確保
* 1024バイトの領域の確保とAで初期化
* 32バイトの領域の確保と0で初期化
* 初期化されていない1024バイトの領域の確保
* 関数テーブルの作成

が行われていた。

それと`malloc`関数で確保された領域は

|heap|address||
|:--:|:--:|:--|
|...|...|ヒープは高位に伸びる|
|24bytes|*0x0804b0e8|いろいろな値、banner.txtとかfunc_greetingとか|
|...|||
|10 bytes||ユーザーネーム|
|...||4バイトの倍数から確実にずれたので、アラインメントされた余分な領域|
|1024 bytes|*0x0804b0a0|Aで埋められている|
|32 bytes|*0x0804b0ec|0で初期化済み|
|1024 bytes|*0x0804b0f0|初期化されていない|

## input_insn関数

そろそろ忘れた人もいるので再掲する。`main`関数の中で`kindvm_setup`関数の次に`input_insn`関数が呼び出されている。

```
 804878c:	e8 9a 08 00 00       	call   804902b <ctf_setup>
 8048791:	e8 5a 00 00 00       	call   80487f0 <kindvm_setup>
 8048796:	e8 28 02 00 00       	call   80489c3 <input_insn>
```

この関数自体はとても短くてとても好感がもてる。前半`printf`あたりは要らない。

```
080489c3 <input_insn>:
 80489c3:	55                   	push   %ebp
 80489c4:	89 e5                	mov    %esp,%ebp
 80489c6:	83 ec 08             	sub    $0x8,%esp
 80489c9:	83 ec 0c             	sub    $0xc,%esp
 80489cc:	68 d0 91 04 08       	push   $0x80491d0
 80489d1:	e8 9a fb ff ff       	call   8048570 <printf@plt>
 80489d6:	83 c4 10             	add    $0x10,%esp
 80489d9:	a1 f0 b0 04 08       	mov    0x804b0f0,%eax
 80489de:	83 ec 04             	sub    $0x4,%esp
 80489e1:	68 00 04 00 00       	push   $0x400
 80489e6:	50                   	push   %eax
 80489e7:	6a 00                	push   $0x0
 80489e9:	e8 72 fb ff ff       	call   8048560 <read@plt>
 80489ee:	83 c4 10             	add    $0x10,%esp
 80489f1:	90                   	nop
 80489f2:	c9                   	leave
 80489f3:	c3                   	ret
```

読むべきはこの部分。`0x0804b0f0`という値を`eax`に入れて、`read`関数の第二引数に渡している。第一引数は0なので標準入力、第三引数は1024だから普通に考えて読み込むサイズ。

```
 80489d9:	a1 f0 b0 04 08       	mov    0x804b0f0,%eax
 80489de:	83 ec 04             	sub    $0x4,%esp
 80489e1:	68 00 04 00 00       	push   $0x400
 80489e6:	50                   	push   %eax
 80489e7:	6a 00                	push   $0x0
 80489e9:	e8 72 fb ff ff       	call   8048560 <read@plt>
```

`0x0804b0f0`は`kindvm_setup`関数内で確保した1024バイトの領域を指し示していたはず。つまり`read`関数でこの領域に標準入力から命令を読み込むらしい。

ひとまず`0x0804b0f0`の使いみちが判明した。となるともう一つの1024バイトはメモリ代わり？

|heap|address||
|:--:|:--:|:--|
|...|...|ヒープは高位に伸びる|
|24bytes|*0x0804b0e8|いろいろな値、banner.txtとかfunc_greetingとか|
|...|||
|10 bytes||ユーザーネーム|
|...||4バイトの倍数から確実にずれたので、アラインメントされた余分な領域|
|1024 bytes|*0x0804b0a0|Aで埋められている、おそらくメモリ？|
|32 bytes|*0x0804b0ec|0で初期化済み|
|1024 bytes|*0x0804b0f0|命令が格納される|
